'use strict';

// include dependencies
const fs = require('fs');

// read json file
let data = fs.readFileSync('level1/data.json');
let cartData = JSON.parse(data);

// declare variables
let searchArticle;
let carts = [];
let output = {};

try {
    // function for searching article
    searchArticle = (article_id) => {
        let result = cartData.articles.filter(function (article) {
            return article.id == article_id;
        });

        // return mapped data
        return result[0];
    };

    // loop iteration for carts value
    cartData.carts.forEach(function (cart) {
        let total = 0; // initialize total on each iteration
        // loop iteration for carts.items
        cart.items.forEach(function (item) {
            // call searchArticle with article_id and assign to variable
            let article = searchArticle(item.article_id);
            total += article.price * item.quantity; // total calculation
        });

        // push values into carts array
        carts.push({
            "id" : cart.id,
            "total" : total
        });
    });

    output.carts = carts; // assign carts to output JSON object
    fs.writeFileSync('level1/output.json', JSON.stringify(output)); // write final output to destination
} catch (e) {
    // error handling
    console.log('Something went wrong.' + e);
}