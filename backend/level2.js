'use strict';

// include dependencies
const fs = require('fs');

// read json file
let data = fs.readFileSync('level2/data.json');
let cartData = JSON.parse(data);

// declare variables
let searchArticle, deliveryFees;
let carts = [];
let output = {};

try {
    // function for searching article
    searchArticle = (article_id) => {
        let result = cartData.articles.filter(function (article) {
            return article.id == article_id;
        });

        // return mapped data
        return result[0];
    };

    // function for mapping delivery fees
    deliveryFees = (total) => {
        let result = cartData.delivery_fees.filter(function (fees) {
            return (total >= fees.eligible_transaction_volume.min_price && (total < fees.eligible_transaction_volume.max_price || fees.eligible_transaction_volume.max_price == null));
        });

        // return mapped data
        return result[0];
    };

    // loop iteration for carts value
    cartData.carts.forEach(function (cart) {
        let total = 0; // initialize total on each iteration
        // loop iteration for carts.items
        cart.items.forEach(function (item) {
            // call searchArticle with article_id and assign to variable
            let article = searchArticle(item.article_id);
            total += article.price * item.quantity; // total calculation
        });

        // call deliveryFees with total values for delivery fees calculation
        let fees = deliveryFees(total);
        total += fees.price; // add delivery fees to total

        // push values into carts array
        carts.push({
            "id" : cart.id,
            "total" : total
        });
    });

    output.carts = carts; // assign carts to output JSON object
    fs.writeFileSync('level2/output.json', JSON.stringify(output)); // write final output to destination
} catch (e) {
    // error handling
    console.log('Something went wrong.' + e);
}